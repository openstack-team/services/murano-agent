Source: murano-agent
Section: net
Priority: optional
Maintainer: Debian OpenStack <team+openstack@tracker.debian.org>
Uploaders:
 Thomas Goirand <zigo@debian.org>,
Build-Depends:
 debhelper-compat (= 11),
 dh-python,
 openstack-pkg-tools,
 python3-all,
 python3-pbr,
 python3-setuptools,
 python3-sphinx,
Build-Depends-Indep:
 python3-coverage,
 python3-cryptography,
 python3-eventlet,
 python3-git,
 python3-hacking,
 python3-kombu,
 python3-openstackdocstheme,
 python3-oslo.config,
 python3-oslo.log,
 python3-oslo.service,
 python3-oslo.utils,
 python3-oslotest,
 python3-requests,
 python3-semantic-version,
 python3-sphinxcontrib.httpdomain,
 python3-stestr,
 python3-testtools,
 python3-unittest2,
 python3-yaml,
 subunit,
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/openstack-team/services/murano-agent
Vcs-Git: https://salsa.debian.org/openstack-team/services/murano-agent.git
Homepage: https://github.com/openstack/murano-agent

Package: murano-agent
Architecture: all
Section: python
Depends:
 adduser,
 python3-cryptography,
 python3-eventlet,
 python3-git,
 python3-kombu,
 python3-oslo.config,
 python3-oslo.log,
 python3-oslo.service,
 python3-oslo.utils,
 python3-pbr,
 python3-requests,
 python3-semantic-version,
 python3-yaml,
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 python3-ldap,
Description: cloud-ready application catalog - VM agent
 Murano Project introduces an application catalog, which allows application
 developers and cloud administrators to publish various cloud-ready
 applications in a browsable categorised catalog, which may be used by the
 cloud users (including the inexperienced ones) to pick-up the needed
 applications and services and composes the reliable environments out of them
 in a "push-the-button" manner.
 .
 This package contains the Murano Agent, which is a VM-side guest agent that
 accepts commands from Murano Conductor and executes them.
